FROM debian:buster-slim

ARG ODOO_VERSION=12.0

ENV LANG C.UTF-8
ENV DB_HOST=db			\
	DB_PORT=5432		\
	DB_USER=odoo		\
	DB_PASSWORD=odoo

EXPOSE 8069 8071 8072

COPY ./odoo-entrypoint.sh /
RUN chmod +x /odoo-entrypoint.sh

WORKDIR /odoo

# Install required packages
RUN apt update && \
	apt install -y --no-install-recommends \
		python3-pip python3-dev libpq-dev build-essential unixodbc-dev postgresql-client zlib1g-dev \
		libxslt1-dev libxml2-dev libldap2-dev libsasl2-dev node-less git curl python3-wheel

# Clean apt cache
RUN apt clean

RUN git clone https://github.com/OCA/OCB.git OCB --depth=1 --branch=$ODOO_VERSION --single-branch

# Install required python modules
RUN pip3 install setuptools && \
	pip3 install --no-cache-dir -r OCB/requirements.txt && \
	pip3 install paramiko watchdog odfpy codicefiscale psycogreen pyodbc

# TODO: Add CUPS packages

# This script wait until postgres becomes available and then starts odoo
RUN curl https://raw.githubusercontent.com/vishnubob/wait-for-it/master/wait-for-it.sh > /wait-for-it.sh && \
	chmod +x /wait-for-it.sh

ENTRYPOINT ["/odoo-entrypoint.sh"]
