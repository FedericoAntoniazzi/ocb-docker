# OCB (in) Docker
Unofficial docker image of Odoo Community Backport 

# How to use this image
## Requirements
- Docker - ([How to install Docker](https://docs.docker.com/get-docker/))
- Docker-compose - ([How to install docker-compose](https://docs.docker.com/compose/install/))

## Pull the image 
This is an optional step because docker (and docker-compose) will automatically pull the image when you run the container

```
docker pull federicoantoniazzi/odoo:<version>
```

## Docker-compose 
Here's a basic docker-compose.yml file you can use to run this image

```
version: '2'

services:

    web:
        image: federicoantoniazzi/odoo:12.0
        container_name: odoo
        depends_on:
            - db
        ports:
            - "8069:8069"
        volumes:
            - ./odooconf/odoo.conf:/odoo/odoo.conf
            # Be sure to add this path in odoo.conf
            - ./addons/:/odoo/addons/
        environment:
            - DB_HOST=db
            - DB_PORT=5432
            - DB_USER=odoo
            - DB_PASSWORD=odoo

    db:
        image: postgres:10
        container_name: postgres
        environment:
            - POSTGRES_DB=postgres
            - POSTGRES_USER=odoo
            - POSTGRES_PASSWORD=odoo
```

Here's another sample just if you want to use docker volumes to save your data

```
version: '2'

services:

    web:
        image: federicoantoniazzi/odoo:12.0
        container_name: odoo
        depends_on:
            - db
        ports:
            - "8069:8069"
        volumes:
            - ./odooconf/odoo.conf:/odoo/odoo.conf
            # Be sure to add this path in odoo.conf
            - ./addons/:/odoo/addons/
        environment:
            - DB_HOST=db
            - DB_PORT=5432
            - DB_USER=odoo
            - DB_PASSWORD=odoo

    db:
        image: postgres:10
        container_name: postgres
        environment:
            - POSTGRES_DB=postgres
            - POSTGRES_USER=odoo
            - POSTGRES_PASSWORD=odoo
            - PGDATA=/var/lib/postgresql/data/pgdata
        volumes:
            - odoo-database:/var/lib/postgresql/data/pgdata

volumes:
    odoo-database:

```

## Using custom configuration
Here it is an example of odoo.conf:
```
[options]
addons_path = /odoo/OCB/addons/,/odoo/addons/openeducat_erp
secure = False
proxy_mode = False
timezone = Europe/Rome
port = 8089
```

If you would like to have an example of an odoo.conf file, you can find it [here](https://gist.github.com/Guidoom/d5db0a76ce669b139271a528a8a2a27f)

# External links
- Odoo Community Association: [GitHub profile](https://github.com/OCA)
- Wait for it: [GitHub Repo](https://github.com/vishnubob/wait-for-it)
