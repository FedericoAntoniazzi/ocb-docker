#!/bin/bash

VERSION="${VERSION:=0}"
versions=($(echo $BRANCHES))
version=${versions[$VERSION]}

echo "=============================="
echo "==> Building $version"
echo "=============================="

# Get the SHA's first 8 characters of the latest commit of OCB
OCB_LAST_COMMIT_SHA=$(git ls-remote https://github.com/OCA/OCB -b $version | head -c 8)

# Get the last tag on the current branch
# Fail if no tags are found (First execution)
LAST_TAG=$(git for-each-ref refs/tags --sort=-taggerdate --format='%(refname)' | sed 's/refs\/tags\///' | grep $version | head -n 1)
LAST_TAG_SHA=$(echo $LAST_TAG | cut -d '-' -f 2)

# If SHAs are different -> create new tag, pull it and generate new docker image
if [ "$OCB_LAST_COMMIT_SHA" != "$LAST_TAG_SHA" ]; then

	NEW_TAG_NAME="$version-$OCB_LAST_COMMIT_SHA"
	echo "==> Creating new tag $NEW_TAG_NAME"

	# Build docker image
	docker build \
		--build-arg ODOO_VERSION=$version \
		--tag $CI_REGISTRY_IMAGE:$version \
		.

	# Update tag for branch
	git tag -a $NEW_TAG_NAME -m "Created by GitLab-CI build"

	# Push changes
	git push origin $NEW_TAG_NAME -o ci.skip

	docker push $CI_REGISTRY_IMAGE:$version

	docker image rm "$DOCKER_IMAGE_NAME:$version"

else 

	echo "==> Already pushed this tag, no need to build the image again"

fi
