#!/bin/sh

# Check if odoo.conf exists
FILE=/odoo/odoo.conf
conf=""
if [ -f "$FILE" ]; then
	conf="-c $FILE"
fi

# Wait until postgres container is up
/wait-for-it.sh -t 0 $DB_HOST:$DB_PORT -- "Postgres is up"

# Start odoo
cmd="/odoo/OCB/odoo-bin --db_host $DB_HOST --db_port $DB_PORT -r $DB_USER -w $DB_PASSWORD $conf"
sh -c "$cmd"
